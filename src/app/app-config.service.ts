import { PlatformLocation } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  private _apiUrl = '';

  constructor(private http: HttpClient, private platformLocation: PlatformLocation) { }

  load(): Promise<any> {
    let res = this.http.get<any>(`${this.baseHref}/assets/app-config.json`, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate'
      }
    });
    
    let promise = firstValueFrom(res)
      .then(data => this.setupApiUrl(data['apiUrl']))
      .catch(err => this.setupDefaultApiUrl());
      
    return promise;
  }

  get apiUrl(): string {
    return this._apiUrl;
  }

  private setupApiUrl(url: string): void {
    url = url.trim();
    if (url) {
      this._apiUrl = url;
    } else {
      this._apiUrl = this.getDefaultUrl();
    }

    this._apiUrl += '/api';
    console.log('API URL = ' + this._apiUrl)
  }

  private getDefaultUrl(): string {
    return location.origin;
  }

  private setupDefaultApiUrl(): void {
    this.setupApiUrl('');
  }

  get baseHref(): string {
    let href = this.platformLocation.getBaseHrefFromDOM();
    return this.removeSlashes(href);
  }

  private removeSlashes(str: string): string {
    console.log(str.replace(/\/+$/, ''));
    return str.replace(/\/+$/, '');
  }
}
