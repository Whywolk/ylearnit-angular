import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './component/admin/admin.component';
import { AdminDashboardComponent } from './component/admin-dashboard/admin-dashboard.component';
import { CourseListComponent } from './component/course-list/course-list.component';
import { CourseDetailsComponent } from './component/course-details/course-details.component';
import { AddCourseComponent } from './component/add-course/add-course.component';
import { TopicDetailsComponent } from './component/topic-details/topic-details.component';

const adminRoutes: Routes = [
  { path: '', component: AdminComponent, children: [
    { path: '', component: AdminDashboardComponent },
    { path: 'courses', component: CourseListComponent },
    { path: 'courses/add', component: AddCourseComponent },
    { path: 'course/:id', component: CourseDetailsComponent },
    { path: 'course/:courseId/:topicId', component: TopicDetailsComponent }
  ]}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
