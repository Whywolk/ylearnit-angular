import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigService } from 'src/app/app-config.service';
import { Course } from '../model/course';
import { Observable } from 'rxjs';
import { Topic } from '../model/topic';
import { TopicElementCard } from '../model/topic-element-card';
import { TopicElement } from '../model/topic-element';
import { ElementCard } from '../model/element-card';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private courseUrl = '/courses';

  constructor(private appConfig: AppConfigService, private http: HttpClient) { }

  // -----------Course-----------

  getCourses(targetLang: string, sourceLang: string): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.getApiUrl()}/${targetLang}/${sourceLang}`);
  }

  getCourse(id: number): Observable<Course> {
    return this.http.get<Course>(`${this.getApiUrl()}/by-id/${id}`);
  }

  createCourse(course: Course): Observable<any> {
    return this.http.post(`${this.getApiUrl()}`, course);
  }

  updateCourse(oldCourse: Course, newCourse: Course): Observable<any> {
    return this.http.put(this.getCourseUrl(oldCourse), newCourse);
  }

  deleteCourse(course: Course): Observable<any> {
    return this.http.delete(this.getCourseUrl(course));
  }

  // -----------Topic-----------

  getTopic(id: number): Observable<Topic> {
    return this.http.get<Topic>(`${this.getApiUrl()}/topic/${id}`);
  }

  createTopic(course: Course, topic: Topic): Observable<any> {
    return this.http.post(this.getCourseUrl(course), topic);
  }

  updateTopic(course: Course, oldTopic: Topic , newTopic: Topic): Observable<any> {
    return this.http.put(`${this.getCourseUrl(course)}/${oldTopic.title}`, newTopic);
  }

  deleteTopic(course: Course, topic: Topic): Observable<any> {
    return this.http.delete(`${this.getCourseUrl(course)}/${topic.title}`);
  }

  // -----------TopicElement-----------

  getTopicElements(course: Course, topic: Topic): Observable<TopicElementCard[]> {
    return this.http.get<TopicElementCard[]>(`${this.getCourseUrl(course)}/${topic.title}/elements`);
  }

  updateTopicElements(course: Course, topic: Topic, topicElements: TopicElement[]): Observable<any> {
    return this.http.put(`${this.getCourseUrl(course)}/${topic.title}/elements`, topicElements);
  }

  deleteTopicElements(course: Course, topic: Topic, topicElements: TopicElement[]): Observable<any> {
    return this.http.delete(`${this.getCourseUrl(course)}/${topic.title}/elements`, { body: topicElements });
  }


  // -----------Find-----------

  find(q: string, course: Course): Observable<ElementCard[]> {
    return this.http.get<ElementCard[]>(`${this.getApiUrl()}/find`, {
      params: {
        q: q,
        targetLang: course.targetLang,
        sourceLang: course.sourceLang
      }
    });
  }

  private getCourseUrl(c: Course): string {
    return `${this.getApiUrl()}/${c.targetLang}/${c.sourceLang}/${c.title}`;
  }

  private getApiUrl(): string {
    return this.appConfig.apiUrl + this.courseUrl;
  }
}
