export interface Side {
    contentId: number;
    content: string;
    contentType: string;
    lang: string;
    sideType: string;
    variants: string[];
    addition: string;
}