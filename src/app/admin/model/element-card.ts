import { Card } from "./card";

export interface ElementCard {
    card: Card;
    elementId: number;
}