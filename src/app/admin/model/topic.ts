export interface Topic {
    id?: number;
    title: string;
    description: string;
    num: number;
    difficult: number;
}