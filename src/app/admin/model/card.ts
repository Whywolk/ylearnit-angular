import { Side } from "./side";

export interface Card {
    sides: Side[];
    dbName: string;
    elementIdDb: number;
}