import { Topic } from "./topic";

export interface Course {
    id?: number;
    title: string;
    description: string;
    targetLang: string;
    sourceLang: string;
    difficult: number;
    topics: Topic[];
}