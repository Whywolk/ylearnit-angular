import { Card } from "./card";
import { TopicElement } from "./topic-element";

export interface TopicElementCard {
    card: Card;
    topicElement: TopicElement;
}