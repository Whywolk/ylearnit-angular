import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';

@Component({
  selector: 'admin-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  isMenuCollapsed = true;

  constructor(private authService: AuthService, private router: Router) { }

  onLogOut(): void {
    console.log('Log out');
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  get username(): string {
    return this.authService.username;
  }
}
