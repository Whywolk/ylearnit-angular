import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../../service/course.service';
import { Course } from '../../model/course';

@Component({
  selector: 'admin-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent {
  course: Course = {
    id: undefined,
    title: '',
    description: '',
    targetLang: '',
    sourceLang: '',
    difficult: 0,
    topics: []
  };

  constructor(
    private courseService: CourseService,
    private route: ActivatedRoute,
    private router: Router) {}

  createCourse(): void {
    if (this.course.title && this.course.targetLang && this.course.sourceLang) {
      this.courseService.createCourse(this.course)
        .subscribe({
          next: (data) => {
            this.course = data;
            this.router.navigate([`../../course/${this.course.id}`], { relativeTo: this.route })
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }
}
