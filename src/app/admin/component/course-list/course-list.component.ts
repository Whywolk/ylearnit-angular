import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../service/course.service';
import { Course } from '../../model/course';

@Component({
  selector: 'admin-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  courses: Course[] = [];
  targetLang: string = '';
  sourceLang: string = '';


  constructor(private courseService: CourseService) { }

  ngOnInit(): void {
  }

  getCourses(): void {
    this.courseService.getCourses(this.targetLang, this.sourceLang)
    .subscribe({
      next: (data) => {
        this.courses = data;
        console.log(data);
      },
      error: (e) => {
        console.error(e);
      }
    });
  }
}
