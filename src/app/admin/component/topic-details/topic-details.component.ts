import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../../service/course.service';
import { Course } from '../../model/course';
import { Topic } from '../../model/topic';
import { TopicElementCard } from '../../model/topic-element-card';
import { ElementCard } from '../../model/element-card';
import { TopicElement } from '../../model/topic-element';
import cloneDeep from 'lodash-es/cloneDeep';

@Component({
  selector: 'admin-topic-details',
  templateUrl: './topic-details.component.html',
  styleUrls: ['./topic-details.component.css']
})
export class TopicDetailsComponent implements OnInit {
  course!: Course;
  topic!: Topic;
  oldTopic!: Topic;
  isChanging = false;

  topicElements?: TopicElementCard[];
  addedElementIds = new Set<number>();
  
  query: string = '';
  resultFind?: ElementCard[];

  isShowTopicElements = true;

  newTopicElements: TopicElement[] = [];
  deletingTopicElements: TopicElement[] = [];

  isCommit = false;

  constructor(
    private courseService: CourseService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    let courseId = this.route.snapshot.params['courseId'];
    let topicId = this.route.snapshot.params['topicId'];
    
    this.loadCourse(courseId, topicId);
  }

  loadCourse(courseId: number, topicId: number): void {
    this.courseService.getCourse(courseId)
      .subscribe({
        next: (data) => {
          this.course = data;
          for (let t of this.course.topics) {
            if (t.id == topicId) {
              this.topic = t;
              this.loadTopicElements(this.course, this.topic);
            }
          }
          console.log(data);
        },
        error: (e) => {
          console.error(e);
        }
      });
  }

  loadTopicElements(course: Course, topic: Topic) {
    this.courseService.getTopicElements(course, topic)
      .subscribe({
        next: (data) => {
          this.topicElements = data;
          for (let te of this.topicElements) {
            this.addedElementIds.add(te.topicElement.elementId);
          }
          console.log(data);
          console.log(this.addedElementIds);
        },
        error: (e) => {
          console.error(e);
        }
      });
  }

  changeTopic(): void {
    if (this.topic) {
      this.oldTopic = cloneDeep(this.topic);
      this.isChanging = true;
    }
  }

  cancel(): void {
    if (this.topic) {
      this.topic = this.oldTopic;
      this.isChanging = false;
    }
  }

  updateTopic(): void {
    if (this.isChanging && this.topic) {
      this.courseService.updateTopic(this.course, this.oldTopic, this.topic)
        .subscribe({
          next: (data) => {
            this.isChanging = false;
            this.course = data;
            if (this.course) {
              for (let t of this.course.topics) {
                if (t.id == this.topic?.id) {
                  this.topic = t;
                }
              }
            }
            console.log(data);
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }

  deleteTopic(): void {
    if (this.topic) {
      this.courseService.deleteTopic(this.course, this.topic)
        .subscribe({
          next: (data) => {
            this.router.navigate([`..`], { relativeTo: this.route });
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }

  addNewElement(elementId: number) {
    let newTopicElement = {
      elementId: elementId,
      num: 0
    }
    this.newTopicElements.push(newTopicElement);
    this.addedElementIds.add(elementId);
    this.isCommit = true;
    console.log('added ' + elementId);
  }

  deleteElement(elementId: number) {
    let deleteTopicElement = {
      elementId: elementId,
      num: 0
    }
    this.deletingTopicElements.push(deleteTopicElement);
    this.addedElementIds.delete(elementId);
    this.isCommit = true;
    console.log('delete ' + elementId);
  }

  commit() {
    if (this.topic) {
      if (this.newTopicElements.length > 0) {
        this.courseService.updateTopicElements(this.course, this.topic, this.newTopicElements)
          .subscribe({
            next: (data) => {
              if (this.topic) {
                this.loadTopicElements(this.course, this.topic);
                this.isCommit = false;
              }
            },
            error: (e) => {
              console.error(e);
            }
          });
      }
      if (this.deletingTopicElements.length > 0) {
        this.courseService.deleteTopicElements(this.course, this.topic, this.deletingTopicElements)
          .subscribe({
            next: (data) => {
              if (this.topic) {
                this.loadTopicElements(this.course, this.topic);
                this.isCommit = false;
              }
            },
            error: (e) => {
              console.error(e);
            }
          });
      }
    }
  }


  find(): void {
    if (this.query && this.query.trim().length > 0 && this.course) {
      this.courseService.find(this.query, this.course)
        .subscribe({
          next: (data) => {
            this.resultFind = data;
            console.log(data);
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }
}
