import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../service/course.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from '../../model/course';
import { Topic } from '../../model/topic';
import cloneDeep from 'lodash-es/cloneDeep';

@Component({
  selector: 'admin-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {
  course?: Course;
  oldCourse!: Course;
  newTopic?: Topic;

  isChanging = false;

  constructor(
    private courseService: CourseService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    let id = this.route.snapshot.params['id'];
    
    this.courseService.getCourse(id)
      .subscribe({
        next: (data) => {
          this.course = data;
          console.log(data);
        },
        error: (e) => {
          console.error(e);
        }
      });
  }

  changeCourse(): void {
    if (this.course) {
      this.oldCourse = cloneDeep(this.course);
      this.isChanging = true;
    }
  }

  cancel(): void {
    if (this.course) {
      this.course = this.oldCourse;
      this.isChanging = false;
    }
  }

  updateCourse(): void {
    if (this.isChanging && this.course) {
      this.courseService.updateCourse(this.oldCourse, this.course)
        .subscribe({
          next: (data) => {
            this.isChanging = false;
            this.course = data;
            console.log(data);
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }

  deleteCourse(): void {
    if (this.course) {
      this.courseService.deleteCourse(this.course)
        .subscribe({
          next: (data) => {
            this.router.navigate([`../../courses`], { relativeTo: this.route });
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }



  addTopic(): void {
    if (this.course) {
      this.newTopic = {
        id: undefined,
        title: '',
        description: '',
        difficult: 0,
        num: this.course.topics.length + 1
      }
    }
  }

  createTopic(): void {
    if (this.course && this.newTopic) {
      this.courseService.createTopic(this.course, this.newTopic)
        .subscribe({
          next: (data) => {
            this.newTopic = undefined;
            this.course = data;
            console.log(data);
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }

  deleteTopic(topic: Topic): void {
    if (this.course && topic) {
      this.courseService.deleteTopic(this.course, topic)
        .subscribe({
          next: (data) => {
            this.course = data;
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }
}
