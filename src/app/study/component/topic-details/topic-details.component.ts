import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService } from '../../service/study.service';
import { LearningCard } from '../../model/learning-card';
import { Course } from '../../model/course';
import { Topic } from '../../model/topic';

@Component({
  selector: 'app-topic-details',
  templateUrl: './topic-details.component.html',
  styleUrls: ['./topic-details.component.css']
})
export class TopicDetailsComponent {
  course!: Course;
  topic!: Topic;
  learningCards: LearningCard[] = [];
  page: number = 0;
  hasPages = true;

  constructor(
    private studyService: StudyService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    let targetLang = this.route.snapshot.params['tl'];
    let sourceLang = this.route.snapshot.params['sl'];
    let courseTitle = this.route.snapshot.params['courseTitle'];
    let topicTitle = this.route.snapshot.params['topicTitle'];

    this.studyService.getCourse(courseTitle, targetLang, sourceLang)
      .subscribe({
        next: (data) => {
          this.course = data;
          this.topic = this.course.getTopic(topicTitle);
          this.getStatsByPage(this.page);
          console.log(data);
        },
        error: (e) => {
          console.error(e);
        }
      });
  }

  getStatsByPage(page: number): void {
    this.studyService.getStats(this.course.title, this.topic.title, this.course.targetLang, this.course.sourceLang, page)
    .subscribe({
      next: (data) => {
        if (data.length > 0) {
          this.learningCards = this.learningCards.concat(data);
          this.page += 1
          console.debug(data);
        } else {
          this.hasPages = false;
        }
      },
      error: (e) => {
        console.error(e);
      }
    });
  }

  getNextPage(): void {
    this.getStatsByPage(this.page);
  }
}
