import { EventEmitter } from "@angular/core";
import { StudyTest } from "../model/study-test";
import { UserAnswer } from "../model/user-answer";

export interface TestComponent {
  studyTest: StudyTest;
  userAnswerEvent: EventEmitter<UserAnswer>;
}