import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService } from '../../service/study.service';
import { ReviewService } from '../../service/review.service';
import { Course } from '../../model/course';

@Component({
  selector: 'study-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  learningCourses?: Course[];

  constructor(
    private studyService: StudyService,
    private reviewService: ReviewService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    // TODO: get lang pairs
    this.studyService.getCourses("", "", true)
      .subscribe({
        next: (data) => {
          this.learningCourses = data;
          console.debug(data);
        },
        error: (e) => {
          console.error(e);
        }
      });
  }

  onReviewByCourse(course: Course): void {
    if (course.added) {
      this.reviewService.onReviewByCourse(course);
      console.debug('Review by course with id=' + course.id);
      this.router.navigate(['/app/test']);
    }
  }

  navigateToCourse(course: Course): void {
    this.router.navigate(['./course', course.targetLang, course.sourceLang, course.title],
      { relativeTo: this.route }
    );
  }
}
