import { Component, EventEmitter, OnInit, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { TestComponent } from '../test.component';
import { StudyTest } from '../../model/study-test';
import { UserAnswer } from '../../model/user-answer';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Side } from '../../model/side';

@Component({
  selector: 'study-write-test',
  templateUrl: './write-test.component.html',
  styleUrls: ['./write-test.component.css']
})
export class WriteTestComponent implements TestComponent, OnInit {
  private WRITE_DELTA_MS = 1000;

  @Input() studyTest!: StudyTest;
  @Output() userAnswerEvent: EventEmitter<UserAnswer> = new EventEmitter<UserAnswer>();

  isShowAnswer = false;
  answer: FormGroup;
  changeTimestamps: Date[] = [];

  constructor(private fb: FormBuilder) {
    this.answer = this.fb.group({
      answer: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.isShowAnswer = false;
    this.changeTimestamps.push(new Date());
  }

  onAnswerChange(value: string): void {
    this.changeTimestamps.push(new Date());
  }

  onAnswerSubmit(): void {
    this.changeTimestamps.push(new Date());
    this.isShowAnswer = true;
  }

  onNext(): void {
    let deltaList = this.calcDeltaList(this.changeTimestamps);

    let totalTime = this.calcTotalTime(deltaList);
    let thinkingTime = this.calcThinkingTime(deltaList);

    let userAnswer = this.buildUserAnswer(this.answer.controls['answer'].value, thinkingTime, totalTime);
    this.userAnswerEvent.emit(userAnswer);
  }

  buildUserAnswer(answer: string, idleSeconds: number, totalSeconds: number): UserAnswer {
    let userAnswerSide = new Side("tag", answer, "TEXT_PLAIN", "en", "USER_ANSWER", []);
    let userAnswer = new UserAnswer(this.studyTest.answers, userAnswerSide, "WRITE", idleSeconds, totalSeconds, this.studyTest.dbName, this.studyTest.elementIdDb, this.studyTest.learningElementId, this.studyTest.repetitionId, "")

    return userAnswer;
  }

  calcDeltaList(timestamps: Date[]): number[] {
    let deltaList: number[] = [];
    for (let i = 0; i < timestamps.length - 1; i++) {
      let t1 = timestamps[i].getTime();
      let t2 = timestamps[i + 1].getTime();
      deltaList.push(t2 - t1);
    }
    return deltaList;
  }

  calcThinkingTime(deltaList: number[]): number {
    let time = 0;
    for (let delta of deltaList) {
      if (delta > this.WRITE_DELTA_MS) {
        time += delta;
      }
    }
    return Math.round(time / 1000);
  }

  calcTotalTime(deltaList: number[]): number {
    let time = 0;
    for (let delta of deltaList) {
      time += delta;
    }
    return Math.round(time / 1000);
  }
}
