import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService } from '../../service/study.service';
import { Course } from '../../model/course';
import { LangPair } from '../../model/lang-pair';
import { LangPairs } from '../../model/lang-pairs';

@Component({
  selector: 'study-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.css']
})
export class CoursesListComponent implements OnInit {
  courses?: Course[];
  targetLang: string = "";
  sourceLang: string = "";
  langPairs!: LangPairs;
  
  constructor(
    private studyService: StudyService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.getLangPairs();
  }

  getCourses(): void {
    this.studyService.getCourses(this.targetLang, this.sourceLang)
      .subscribe({
        next: (data) => {
          this.courses = data;
          console.debug(data);
        },
        error: (e) => console.error(e)
      }
    );
  }

  getLangPairs(): void {
    // let pairs = [
    //   {tl: "en", sl: "ru"},
    //   {tl: "de", sl: "ru"},
    //   {tl: "ja", sl: "en"},
    //   {tl: "ru", sl: "en"}
    // ]
    // this.langPairs = new LangPairs(pairs);
    
    this.studyService.getLangPairs()
      .subscribe({
        next: (data) => {
          this.langPairs = new LangPairs(data);
          console.debug(data);
        },
        error: (e) => {
          console.error(e);
        }
    });
  }

  navigateToCourse(course: Course): void {
    this.router.navigate(['../course', course.targetLang, course.sourceLang, course.title],
      { relativeTo: this.route }
    );
  }
}
