import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../../service/review.service';
import { StudyTest } from '../../model/study-test';
import { UserAnswer } from '../../model/user-answer';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'study-study-tests',
  templateUrl: './study-tests.component.html',
  styleUrls: ['./study-tests.component.css']
})
export class StudyTestsComponent implements OnInit {
  curTest!: StudyTest;
  curTestNumber = 0;
  isShowTest = false;
  isFinish = false;
  noTests = false;
  shuffle = false;

  constructor(
    public reviewService: ReviewService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    if (this.reviewService.getTestsCount() == 0) {
      console.debug('onSetup');
      return;
    }

    if (this.reviewService.finished) {
      this.reviewService.setup();
      console.debug('onReview');
    }

    if (!this.reviewService.finished && this.reviewService.getTestsCount() > 0) {
      this.curTest = this.reviewService.getCurTest();
      this.curTestNumber = this.reviewService.getCurTestNumber();
      this.isShowTest = true;
      console.debug('continue review');
    }
  }

  getStudyTests(): void {
    this.reviewService.getStudyTests()
      .subscribe({
        next: (data) => {
          if (this.reviewService.getTestsCount() == 0) {
            this.noTests = true;
          } else {
            this.curTest = this.reviewService.getCurTest();
            this.isShowTest = true;
          }
        }
      });
  }

  endStudyTests() {
    this.reviewService.endStudyTests()
      .subscribe({
        next: (data) => {
          this.isFinish = this.reviewService.finished;
          this.router.navigate(['app/test/results']);
          console.debug('Send!');
        },
        error: (e) => {
          console.error(e);
        }
      });
  }

  addUserAnswer(userAnswer: UserAnswer): void {
    console.debug(userAnswer);
    this.reviewService.addAnswer(userAnswer);
    this.isShowTest = false;

    if (this.reviewService.hasNextTest()) {
      this.curTest = this.reviewService.getNextTest();
      this.curTestNumber = this.reviewService.getCurTestNumber();
      console.debug(this.curTestNumber);
      setTimeout(() => {this.isShowTest = true}, 50);
    } else {
      this.endStudyTests();
    }
  }

  onFinish(): void {
    this.endStudyTests();
  }
}
