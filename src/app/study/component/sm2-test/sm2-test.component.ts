import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { TestComponent } from '../test.component';
import { StudyTest } from '../../model/study-test';
import { UserAnswer } from '../../model/user-answer';
import { Side } from '../../model/side';

@Component({
  selector: 'study-sm2-test',
  templateUrl: './sm2-test.component.html',
  styleUrls: ['./sm2-test.component.css']
})
export class SM2TestComponent implements TestComponent, OnInit {
  @Input() studyTest!: StudyTest;
  @Output() userAnswerEvent: EventEmitter<UserAnswer> = new EventEmitter<UserAnswer>();

  isShowAnswer = false;

  sm2Marks = ['0', '1', '2', '3', '4', '5'];
  curMark?: string;

  start!: Date;
  end!: Date;

  constructor() { }

  ngOnInit(): void {
    this.start = new Date();
    this.isShowAnswer = false;
    this.curMark = undefined;

    console.debug("Start=" + this.start);
  }

  showAnswer(): void {
    this.isShowAnswer = true;
    this.end = new Date();

    console.debug("End=" + this.end);
    console.debug("Total=", this.end.getTime() - this.start.getTime())
  }

  onMark(mark: string): void {
    this.curMark = mark;
    this.onNext();
  }

  onNext(): void {
    if (this.curMark) {
      let totalTime = Math.round((this.end.getTime() - this.start.getTime()) / 1000)
      let thinkingTime = totalTime;
      let userAnswer = this.buildUserAnswer(this.curMark, thinkingTime, totalTime);
      this.userAnswerEvent.emit(userAnswer);
    }
  }

  buildUserAnswer(mark: string, idleSeconds: number, totalSeconds: number): UserAnswer {
    let userAnswerSide = new Side("tag", mark, "TEXT_PLAIN", "en", "USER_ANSWER", []);
    let userAnswer = new UserAnswer(this.studyTest.answers, userAnswerSide, "SM2", idleSeconds, totalSeconds, this.studyTest.dbName, this.studyTest.elementIdDb, this.studyTest.learningElementId, this.studyTest.repetitionId, "")

    return userAnswer;
  }
}
