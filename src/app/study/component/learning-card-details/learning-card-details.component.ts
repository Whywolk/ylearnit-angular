import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService } from '../../service/study.service';
import { DetailedLearningCard } from '../../model/detailed-learning-card';

@Component({
  selector: 'study-learning-card-details',
  templateUrl: './learning-card-details.component.html',
  styleUrls: ['./learning-card-details.component.css']
})
export class LearningCardDetailsComponent implements OnInit {
  leId?: number;
  sourceLang?: string;
  learningCard?: DetailedLearningCard;

  constructor(
    private studyService: StudyService,
    private route: ActivatedRoute,
    private router: Router
    ) { }
  
  ngOnInit(): void {
    this.leId = this.route.snapshot.params['lElementId'];
    console.debug(this.route.snapshot.queryParamMap.get('sourceLang'));
    this.sourceLang = this.route.snapshot.queryParamMap.get('sourceLang')!;
    if (this.leId && this.sourceLang) {
      this.studyService.getStatsByIds([this.leId], this.sourceLang)
        .subscribe({
          next: (data) => {
            if (data.length > 0) {
              this.learningCard = data[0];
              console.debug(data);
            }
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }
}
