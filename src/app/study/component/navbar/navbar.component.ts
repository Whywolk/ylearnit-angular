import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';

@Component({
  selector: 'study-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  brand = 'YLearnIt';
  isMenuCollapsed = true;

  constructor(private authService: AuthService, private router: Router) {
    this.easter();
  }

  logOut(): void {
    console.log('Log out');
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  easter() {
    if (Math.random() < 0.1) {
      this.brand = 'よし! LearnIt!'
    }
  }

  get username(): string {
    return this.authService.username;
  }
}
