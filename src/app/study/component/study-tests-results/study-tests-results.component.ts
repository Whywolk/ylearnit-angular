import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../../service/review.service';
import { StudyService } from '../../service/study.service';
import { Card } from '../../model/card';
import { DetailedLearningCard } from '../../model/detailed-learning-card';
import { ActivatedRoute, Router } from '@angular/router';
import { UserAnswer } from '../../model/user-answer';

@Component({
  selector: 'app-study-tests-results',
  templateUrl: './study-tests-results.component.html',
  styleUrls: ['./study-tests-results.component.css']
})
export class StudyTestsResultsComponent implements OnInit {
  results!: DetailedLearningCard[];
  resultsMap = new Map<number, DetailedLearningCard>();

  constructor(
    public reviewService: ReviewService,
    private studyService: StudyService,
    private route: ActivatedRoute,
    private router: Router) { }
  
  ngOnInit(): void {
    if (this.reviewService.finished) {
      this.getResults();
    }
  }

  getResults(): void {
    let leIds = this.reviewService.answers.map(answer => answer.learningElementId);

    if (this.reviewService.sourceLang) {
      this.studyService.getStatsByIds(leIds, this.reviewService.sourceLang)
        .subscribe({
          next: (data) => {
            this.results = data;
            for (let result of this.results) {
              if (leIds.includes(result.learningElement.id)) {
                this.resultsMap.set(result.learningElement.id, result);
              }
            }
            console.debug(data);
          },
          error: (e) => {
            console.error(e);
          }
        });
    }
  }

  getContent(card: Card, lang: string): string | undefined {
    for (let side of card.sides) {
      if (side.lang === lang) {
        return side.content;
      }
    }
    return;
  }

  getLocaleDateString(date: Date): string {
    let repDate = new Date(date)
    let curDate = new Date();
    
    let diffTime = repDate.getTime() - curDate.getTime()
    let diffDay = Math.ceil(diffTime / (24 * 60 * 60 * 1000))
    if (diffDay == 0) {
      return 'Сегодня'
    } else if (diffDay == 1) {
      return 'Завтра'
    } else if (diffDay <= 31 && diffDay > 0) {
      return `Через ${diffDay} дней`
    } else {
      return repDate.toLocaleDateString()
    }
  }

  getCard(leId: number): DetailedLearningCard {
    return this.resultsMap.get(leId)!;
  }

  navigateElement(answer: UserAnswer): void {
    this.router.navigate(['/app/stats/lelement', answer.learningElementId], {
      queryParams: {
        sourceLang: this.reviewService.sourceLang
      }
    });
  }
}
