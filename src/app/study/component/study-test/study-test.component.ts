import { Component, EventEmitter, Input, OnInit, Output, Type, ViewChild } from '@angular/core';
import { StudyTest } from '../../model/study-test';
import { UserAnswer } from '../../model/user-answer';
import { SM2TestComponent } from '../sm2-test/sm2-test.component';
import { TestComponent } from '../test.component';
import { TestDirective } from '../../answer.directive';
import { WriteTestComponent } from '../write-test/write-test.component';

@Component({
  selector: 'study-study-test',
  templateUrl: './study-test.component.html',
  styleUrls: ['./study-test.component.css']
})
export class StudyTestComponent implements OnInit {
  @Input() studyTest!: StudyTest;
  @Output() userAnswerEvent = new EventEmitter<UserAnswer>();
  @ViewChild(TestDirective, {static: true}) test!: TestDirective;
  
  ngOnInit(): void {
    console.debug('study-test-component init');
    this.loadTest();
  }

  ngAfterViewInit() {
    // this.loadTest();
  }

  loadTest(): void {
    let testComponent: Type<any>;
    switch(this.studyTest.type) {
      case 'SM2':
        console.debug('sm2');
        testComponent = SM2TestComponent;
        break;
      case 'WRITE':
        console.debug('write');
        testComponent = WriteTestComponent;
        break;
      default:
        console.debug('default - sm2');
        testComponent = SM2TestComponent;
        break;
    }
    console.log(1);
    this.test.viewContainerRef.clear();

    console.log(2);
    let componentRef = this.test.viewContainerRef.createComponent<TestComponent>(testComponent);

    console.log(3);
    componentRef.instance.studyTest = this.studyTest;
    componentRef.instance.userAnswerEvent.subscribe(value => this.addAnswer(value));
  }

  addAnswer(userAnswer: UserAnswer): void {
    console.debug('answer event');
    this.userAnswerEvent.emit(userAnswer);
  }
}
