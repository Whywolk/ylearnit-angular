import { Component, Input } from '@angular/core';
import { Card } from '../../model/card';
import { LearningCard } from '../../model/learning-card';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'study-learning-card-list',
  templateUrl: './learning-card-list.component.html',
  styleUrls: ['./learning-card-list.component.css']
})
export class LearningCardListComponent {
  @Input() learningCards: LearningCard[] = [];
  @Input() sourceLang!: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router
    ) { }

  getContent(card: Card, lang: string): string | undefined {
    for (let side of card.sides) {
      if (side.lang === lang) {
        return side.content;
      }
    }
    return;
  }

  getLocaleDateString(date: Date): string {
    let repDate = new Date(date)
    let curDate = new Date();
    
    let diffTime = repDate.getTime() - curDate.getTime()
    let diffDay = Math.ceil(diffTime / (24 * 60 * 60 * 1000))
    if (diffDay == 0 ) {
      return 'Сегодня'
    } else if (diffDay == 1) {
      return 'Завтра'
    } else if (diffDay <= 31 && diffDay > 0) {
      return `Через ${diffDay} дней`
    } else {
      return repDate.toLocaleDateString()
    }
  }

  navigateElement(card: LearningCard): void {
    this.router.navigate(["/app/stats/lelement", card.learningElement.id], {
      queryParams: {
        sourceLang: this.sourceLang
      }
    });
  }
}
