import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudyService } from '../../service/study.service';
import { ReviewService } from '../../service/review.service';
import { Course } from '../../model/course';
import { Topic } from '../../model/topic';

@Component({
  selector: 'study-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {
  course?: Course;
  isDisabledButtons = false;

  constructor(
    private studyService: StudyService,
    private reviewService: ReviewService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    let targetLang = this.route.snapshot.params['tl'];
    let sourceLang = this.route.snapshot.params['sl'];
    let title = this.route.snapshot.params['courseTitle'];
    this.getCourse(title, targetLang, sourceLang);
  }

  getCourse(title: string, targetLang: string, sourceLang: string) {
    this.studyService.getCourse(title, targetLang, sourceLang)
      .subscribe({
        next: (data) => {
          this.course = data;
          console.log(data);
        },
        error: (e) => {
          console.error(e);
        }
      });
  }

  addCourse(): void {
    if (this.course) {
      this.isDisabledButtons = true;
      this.studyService.addCourse(this.course.title, this.course.targetLang, this.course.sourceLang)
        .subscribe({
          next: (data) => {
            this.ngOnInit();
            this.isDisabledButtons = false;
          },
          error: (e) => {
            console.log(e);
          }
        });
    }
  }

  addTopic(topic: Topic) {
    if (this.course && this.course.added && topic) {
      this.isDisabledButtons = true;
      this.studyService.addTopic(this.course.title, topic.title, this.course.targetLang, this.course.sourceLang)
        .subscribe({
          next: (data) => {
            this.ngOnInit();
            this.isDisabledButtons = false;
          },
          error: (e) => {
            console.log(e);
          }
        });
    }
  }

  onReviewByCourse(course: Course): void {
    if (course.added) {
      this.reviewService.onReviewByCourse(course);
      console.debug('by course=' + course.lCourseId);
      this.router.navigate(['/app/test']);
    }
  }

  onReviewByTopic(course: Course, topic: Topic): void {
    if (course.added && topic.added) {
      this.reviewService.onReviewByTopic(course, topic);
      console.debug('by topic=' + topic.lTopicId);
      this.router.navigate(['/app/test']);
    }
  }

  navigateToStats(topic: Topic): void {
    this.router.navigate(['./', topic.title], { relativeTo: this.route });
  }
}
