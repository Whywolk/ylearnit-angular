import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/guard/auth.guard';
import { CourseDetailsComponent } from './component/course-details/course-details.component';
import { CoursesListComponent } from './component/courses-list/courses-list.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { HomeComponent } from './component/home/home.component';
import { LearningCardDetailsComponent } from './component/learning-card-details/learning-card-details.component';
import { StudyTestsComponent } from './component/study-tests/study-tests.component';
import { StudyTestsResultsComponent } from './component/study-tests-results/study-tests-results.component';
import { TopicDetailsComponent } from './component/topic-details/topic-details.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard], children: [
    { path: '', component: DashboardComponent },
    { path: 'courses', component: CoursesListComponent },
    { path: 'course/:tl/:sl/:courseTitle', component: CourseDetailsComponent },
    { path: 'course/:tl/:sl/:courseTitle/:topicTitle', component: TopicDetailsComponent },
    { path: 'test', component: StudyTestsComponent },
    { path: 'test/results', component: StudyTestsResultsComponent },
    { path: 'stats/lelement/:lElementId', component: LearningCardDetailsComponent }
  ]},
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class StudyRoutingModule { }
