import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, tap } from 'rxjs';
import { AppConfigService } from 'src/app/app-config.service';
import { Course } from '../model/course';
import { StudyTest } from '../model/study-test';
import { UserAnswer } from '../model/user-answer';
import { LearningCard } from '../model/learning-card';
import { DetailedLearningCard } from '../model/detailed-learning-card';
import { LearningElement } from '../model/learning-element';
import { DetailedLearningElement } from '../model/detailed-learning-element';
import { ReviewParams } from '../model/review-params';
import { LangPair } from '../model/lang-pair';


@Injectable({
  providedIn: 'root'
})
export class StudyService {
  private studyUrl = '/study';
  private reviewUrl = '/review';

  constructor(private appConfig: AppConfigService, private http: HttpClient) { }

  getCourse(title: string, targetLang: string, sourceLang: string, learning=false): Observable<Course> {
    return this.http.get<Course>(
      `${this.studyApiUrl}/courses/${targetLang}/${sourceLang}/${title}`, {
        params: {
          'learning': learning
        }
      })
      .pipe(
        map((data: any) => Course.toModel(data))
      );
  }

  getCourses(targetLang: string | undefined = "", sourceLang: string | undefined = "", learning=false): Observable<Course[]> {
    let url = `${this.studyApiUrl}/courses`;
    
    if (targetLang) {
      url += `/${targetLang}`;
      if (sourceLang) {
        url += `/${sourceLang}`;
      }
    }
    return this.http.get<Course[]>(url, {
      params: {
        'learning': learning
      }})
      .pipe(
        map((data: any) => {
          return data.map((dto: any) => Course.toModel(dto));
        })
    );
  }

  addCourse(title: string, targetLang: string, sourceLang: string): Observable<any> {
    return this.http.post<any>(
      `${this.studyApiUrl}/courses/${targetLang}/${sourceLang}/${title}`, {}
    );
  }

  addTopic(courseTitle: string, topicTitle: string, targetLang: string, sourceLang: string): Observable<any> {
    return this.http.post<any>(
      `${this.studyApiUrl}/courses/${targetLang}/${sourceLang}/${courseTitle}/${topicTitle}`, {}
    );
  }



  getStudyTests(reviewParams: ReviewParams): Observable<StudyTest[]> {
    return this.http.post<StudyTest[]>(`${this.reviewApiUrl}/tests`, reviewParams);
  }

  endStudyTests(userAnswers: UserAnswer[]): Observable<any> {
    return this.http.post<any>(`${this.reviewApiUrl}/answers`, userAnswers);
  }



  getStatsByIds(learningElementIds: number[], sourceLang: string): Observable<DetailedLearningCard[]> {
    return this.http.get<DetailedLearningCard[]>(`${this.studyApiUrl}/stats`, {
        params: { 
            'leIds': learningElementIds,
            'sourceLang': sourceLang
          }
      })
      .pipe(
        map((data: any) => {
          return data.map((dto: any) => {
            let learningCard = dto;
            learningCard.learningElement = new DetailedLearningElement(learningCard.learningElement);
            return learningCard;
          })
        })
      );
  }

  getStats(courseTitle: string, topicTitle: string, targetLang: string, sourceLang: string, page: number = 0): Observable<LearningCard[]> {
    return this.http.get<LearningCard[]>(
      `${this.studyApiUrl}/stats/${targetLang}/${sourceLang}/${courseTitle}/${topicTitle}`, {
      params: {
        'page': page
      }})
      .pipe(
        map((data: any) => {
          let elements = data;
          return elements.map((dto: any) => {
            let learningCard = dto;
            learningCard.learningElement = new LearningElement(learningCard.learningElement);
            return learningCard;
          })
        })
      );
  }

  getLangPairs(learning: boolean = false): Observable<LangPair[]> {
    return this.http.get<LangPair[]>(`${this.studyApiUrl}/langpairs`, {
      params: {
        'learning': learning
      }});
  }

  get studyApiUrl(): string {
    return this.appConfig.apiUrl + this.studyUrl;
  }

  get reviewApiUrl(): string {
    return this.appConfig.apiUrl + this.reviewUrl;
  }
}
