import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { StudyService } from './study.service';
import { StudyTest } from '../model/study-test';
import { UserAnswer } from '../model/user-answer';
import { Course } from '../model/course';
import { Topic } from '../model/topic';
import { ReviewParams } from '../model/review-params';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  private _testTypes = ['SM2', 'WRITE'];
  private _tests: StudyTest[] = [];
  private _answers: UserAnswer[] = [];
  private _curTestNumber: number = 0;
  private _finished = false;
  private _reviewParams!: ReviewParams;
  
  constructor(private studyService: StudyService) {
    this.setup();
  }

  getStudyTests(): Observable<any> {
    if (!(this._reviewParams.targetLang && this._reviewParams.sourceLang)) {
      throw new Error('Target lang or source lang not setted');
    } else {
      return this.studyService.getStudyTests(this._reviewParams)
        .pipe(
          tap((data) => {
            this._tests = data;
            console.debug(data);
      }));
    }
  }

  endStudyTests(): Observable<any> {
    return this.studyService.endStudyTests(this._answers)
      .pipe(
        tap(data => {
          this._finished = true;
      }));
  }

  onReviewByCourse(course: Course): void {
    this.setup();

    this._reviewParams.targetLang = course.targetLang;
    this._reviewParams.sourceLang = course.sourceLang;

    this._reviewParams.courseTitle = course.title;
    this._reviewParams.topicTitles = course.topics
      .filter(topic => topic.added)
      .map(topic => topic.title);
  }

  onReviewByTopic(course: Course, topic: Topic) {
    this.setup();
    if (! course.containsTopic(topic)) {
      return;
    }
    
    this._reviewParams.targetLang = course.targetLang;
    this._reviewParams.sourceLang = course.sourceLang;

    this._reviewParams.courseTitle = course.title;
    this._reviewParams.topicTitles = [topic.title];
  }

  setup(): void {
    this._tests = [];
    this._answers = [];
    this._curTestNumber = 0;
    this._finished = false;
    this._reviewParams = new ReviewParams('', '');
  }

  getTestsCount(): number {
    return this._tests.length;
  }

  getTestNumber(test: StudyTest): number {
    return this._tests.indexOf(test);
  }

  getTest(idx: number): StudyTest {
    return this._tests[idx];
  }

  getCurTest(): StudyTest {
    return this._tests[this._curTestNumber];
  }

  getCurTestNumber(): number {
    return this._curTestNumber;
  }

  getNextTest(): StudyTest {
    if (this.hasNextTest()) {
      this._curTestNumber += 1;
    }
    return this._tests[this._curTestNumber];
  }

  hasNextTest(): boolean {
    if (this._curTestNumber + 1 < this.getTestsCount()) {
      return true;
    } else {
      return false;
    }
  }

  get tests(): StudyTest[] {
    return this._tests;
  }


  addAnswer(userAnswer: UserAnswer): void {
    this._answers.push(userAnswer);
  }

  get testTypes(): string[] {
    return this._testTypes;
  }

  get answers(): UserAnswer[] {
    return this._answers;
  }

  get targetLang(): string {
    return this._reviewParams.targetLang;
  }

  set targetLang(value: string) {
    this._reviewParams.targetLang = value;
  }

  get sourceLang(): string {
    return this._reviewParams.sourceLang;
  }

  set sourceLang(value: string) {
    this._reviewParams.sourceLang = value;
  }

  get finished(): boolean {
    return this._finished;
  }

  get type(): string {
    return this._reviewParams.testType;
  }

  set type(value: string) {
    this._reviewParams.testType = value;
  }

  get shuffle(): boolean {
    return this._reviewParams.shuffle;
  }

  set shuffle(value: boolean) {
    this._reviewParams.shuffle = value;
  }
}
