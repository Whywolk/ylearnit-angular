import { Answer, AnswerDto } from "./answer";

export interface RepetitionDto {
  id: number;
  loop: number;
  serialNumber: number;
  date: Date;
  interval: number;
  answer: AnswerDto;
}

export class Repetition {
  id: number;
  loop: number;
  serialNumber: number;
  date: Date;
  interval: number;
  answer?: Answer;

  constructor(dto: RepetitionDto) {
    this.id = dto.id;
    this.loop = dto.loop;
    this.serialNumber = dto.serialNumber;
    this.date = new Date(dto.date);
    this.interval = dto.interval;
    if (dto.answer) {
      this.answer = new Answer(dto.answer);
    }
  }
}