export interface AnswerDto {
  id: number;
  accuracy: number;
  retrievability: number;
  idleSeconds: number;
  totalSeconds: number;
  date: Date;
}

export class Answer {
  id: number;
  accuracy: number;
  retrievability: number;
  thinkingTime: number;
  totalTime: number;
  date: Date;

  constructor(dto: AnswerDto) {
    this.id = dto.id;
    this.accuracy = dto.accuracy;
    this.retrievability = dto.retrievability;
    this.thinkingTime = dto.idleSeconds;
    this.totalTime = dto.totalSeconds;
    this.date = new Date(dto.date);
  }
}