import { Card } from "./card";
import { DetailedLearningElement } from "./detailed-learning-element";

export interface DetailedLearningCard {
  learningElement: DetailedLearningElement;
  card: Card;
}