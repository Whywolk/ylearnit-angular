export interface Element {
  id: number;
  targetLang: string;
}