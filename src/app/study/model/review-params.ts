export class ReviewParams {
  targetLang: string;
  sourceLang: string;
  testType: string = 'SM2';
  shuffle: boolean = false;
  courseTitle?: string;
  topicTitles?: string[];

  constructor(targetLang: string, sourceLang: string) {
    this.targetLang = targetLang;
    this.sourceLang = sourceLang;
  }
}
