export class Side {
  tag: string;
  content: string;
  contentType: string;
  lang: string;
  sideType: string;
  variants: string[];

  constructor(tag: string, content: string, contentType: string, lang: string, sideType: string, variants: string[]) {
    this.tag = tag;
    this.content = content;
    this.contentType = contentType;
    this.lang = lang;
    this.sideType = sideType;
    this.variants = variants;
  }
}
