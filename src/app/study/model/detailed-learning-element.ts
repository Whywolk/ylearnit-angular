import { Element } from "./element";
import { Repetition, RepetitionDto } from "./repetition";

export interface DetailedLearningElementDto {
  id: number;
  element: Element;
  nextRep: RepetitionDto;
  lastRep: RepetitionDto;
  repetitions: RepetitionDto[];
}

export class DetailedLearningElement {
  id: number;
  element: Element;
  lastRep?: Repetition;
  nextRep: Repetition;
  repetitions: Repetition[];

  constructor(dto: DetailedLearningElementDto) {
    this.id = dto.id;
    this.element = dto.element;
    if (dto.lastRep) {
      this.lastRep = new Repetition(dto.lastRep);
    }
    this.nextRep = new Repetition(dto.nextRep);
    this.repetitions = dto.repetitions.map((rep: RepetitionDto) => {
      return new Repetition(rep)
    });
  }
}