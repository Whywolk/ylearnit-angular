import { Card } from "./card";
import { LearningElement } from "./learning-element";

export interface LearningCard {
  learningElement: LearningElement;
  card: Card;
}