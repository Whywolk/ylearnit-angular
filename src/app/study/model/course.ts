import { Topic, TopicDto } from "./topic";

export interface CourseDto {
  id: number;
  title: string;
  description: string;
  targetLang: string;
  sourceLang: string;
  difficult: number;
  added: boolean;
  lCourseId: number;
  topics: TopicDto[];
}

export class Course {
  private _id: number;
  private _title: string;
  private _description: string;
  private _targetLang: string;
  private _sourceLang: string;
  private _difficult: number;
  private _isAdded: boolean = false;
  private _lCourseId: number = 0;
  private _topics: Topic[];

  constructor(id: number, title: string, description: string, targetLang: string, sourceLang: string, difficult: number, topics: Topic[]) {
    this._id = id;
    this._title = title;
    this._description = description;
    this._targetLang = targetLang;
    this._sourceLang = sourceLang;
    this._difficult = difficult;
    this._topics = topics;
  }

  static toModel(dto: CourseDto): Course {
    let topics = dto.topics.map(topicDto => Topic.toModel(topicDto));
    let course = new Course(dto.id, dto.title, dto.description, dto.targetLang, dto.sourceLang, dto.difficult, topics);
    course._isAdded = dto.added;
    course._lCourseId = dto.lCourseId; 
    return course;
  }

  containsTopic(topic: Topic): boolean {
    if (this._topics.indexOf(topic) == -1) {
      return false;
    } else {
      return true;
    }
  }

  getTopic(title: string): Topic {
    for (let topic of this.topics) {
      if (topic.title == title) {
        return topic;
      }
    }
    throw new Error(`Topic ${title} not found`);
  }

  get topicCount(): number {
    return this.topics.length;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get title(): string {
    return this._title;
  }
  
  set title(value: string) {
    this._title = value;
  }

  get description(): string {
    return this._description;
  }
  
  set description(value: string) {
    this._description = value;
  }

  get targetLang(): string {
    return this._targetLang;
  }
  
  set targetLang(value: string) {
    this._targetLang = value;
  }

  get sourceLang(): string {
    return this._sourceLang;
  }

  set sourceLang(value: string) {
    this._sourceLang = value;
  }

  get added(): boolean {
    return this._isAdded;
  }

  set added(value: boolean) {
    this._isAdded = value;
  }

  get lCourseId(): number {
    return this._lCourseId;
  }

  set lCourseId(value: number) {
    this._lCourseId = value;
  }

  get difficult(): number {
    return this._difficult;
  }

  set difficult(value: number) {
    this._difficult = value;
  }

  get topics(): Topic[] {
    return this._topics;
  }

  set topics(value: Topic[]) {
    this._topics = value;
  }
}
