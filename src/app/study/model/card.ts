import { Side } from "./side";

export class Card {
  private _dbName: string;
  private _elementIdDb: number;
  private _sides: Side[];

  constructor(dbName: string, elementId: number, sides: Side[]) {
    this._dbName = dbName;
    this._elementIdDb = elementId;
    this._sides = sides;
  }

  get dbName(): string {
    return this._dbName;
  }
  
  set dbName(value: string) {
    this._dbName = value;
  }
  
  get elementIdDb(): number {
    return this._elementIdDb;
  }
  
  set elementIdDb(value: number) {
    this._elementIdDb = value;
  }

  get sides(): Side[] {
    return this._sides;
  }

  set sides(value: Side[]) {
    this._sides = value;
  }
}
