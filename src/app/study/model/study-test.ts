import { Side } from "./side";

export class StudyTest {
  private _question: Side;
  private _answers: Side[];
  private _type: string;
  private _dbName: string;
  private _elementIdDb: number;
  private _learningElementId: number;
  private _repetitionId: number;

  constructor(question: Side, answers: Side[], type: string, _dbName: string, elementIdDb: number, learningElementId: number, repetitionId: number) {
    this._question = question;
    this._answers = answers;
    this._type = type;
    this._dbName = _dbName;
    this._elementIdDb = elementIdDb;
    this._learningElementId = learningElementId;
    this._repetitionId = repetitionId;
  }

  get question(): Side {
    return this._question;
  }

  set question(value: Side) {
    this._question = value;
  }

  get answers(): Side[] {
    return this._answers;
  }

  set answers(value: Side[]) {
    this._answers = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get dbName(): string {
    return this._dbName;
  }

  set dbName(value: string) {
    this._dbName = value;
  }

  get elementIdDb(): number {
    return this._elementIdDb;
  }

  set elementIdDb(value: number) {
    this._elementIdDb = value;
  }

  get learningElementId(): number {
    return this._learningElementId;
  }

  set learningElementId(value: number) {
    this._learningElementId = value;
  }

  get repetitionId(): number {
    return this._repetitionId;
  }

  set repetitionId(value: number) {
    this._repetitionId = value;
  }
}
