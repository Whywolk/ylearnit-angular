import { LangPair } from "./lang-pair";

export class LangPairs {
  pairs: LangPair[] = [];

  constructor(pairs: LangPair[]) {
    this.pairs = pairs;
  }

  getTargetLangs(sourceLang: string): string[] {
    let langs = [""];
    for (let pair of this.pairs) {
      if (!sourceLang || pair.sourceLang === sourceLang) {
        if (! langs.includes(pair.targetLang)) {
          langs.push(pair.targetLang);
        }
      }
    }
    return langs;
  }

  getSourceLangs(targetLang: string): string[] {
    let langs = [""];
    for (let pair of this.pairs) {
      if (!targetLang || pair.targetLang === targetLang) {
        if (! langs.includes(pair.sourceLang)) {
          langs.push(pair.sourceLang);
        }
      }
    }
    return langs;
  }
}