export interface LangPair {
  targetLang: string;
  sourceLang: string;
}