import { Side } from "./side";

export class UserAnswer {
  testAnswers: Side[];
  userAnswer: Side;
  type: string;
  thinkingTime: number;
  totalTime: number;
  dbName: string;
  elementIdDb: number;
  learningElementId: number;
  repetitionId: number;
  data: string;

  constructor(testAnswers: Side[], userAnswer: Side, type: string, thinkingTime: number, totalTime: number, dbName: string, elementIdDb: number, learningElementId: number, repetitionId: number, data: string) {
    this.testAnswers = testAnswers;
    this.userAnswer = userAnswer;
    this.type = type;
    this.thinkingTime = thinkingTime;
    this.totalTime = totalTime;
    this.dbName = dbName;
    this.elementIdDb = elementIdDb;
    this.learningElementId = learningElementId;
    this.repetitionId = repetitionId;
    this.data = data;
  }
}
