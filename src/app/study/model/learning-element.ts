import { Element } from "./element";
import { Repetition, RepetitionDto } from "./repetition";

export interface LearningElementDto {
  id: number;
  element: Element;
  lastRep?: RepetitionDto;
  nextRep: RepetitionDto;
}

export class LearningElement {
  id: number;
  element: Element;
  lastRep?: Repetition;
  nextRep: Repetition;

  constructor(dto: LearningElementDto) {
    this.id = dto.id;
    this.element = dto.element;
    if (dto.lastRep) {
      this.lastRep = new Repetition(dto.lastRep);
    }
    this.nextRep = new Repetition(dto.nextRep);
  }
}