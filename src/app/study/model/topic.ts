export interface TopicDto {
  id: number;
  title: string;
  description: string;
  number: number;
  difficult: number;
  added: boolean;
  lTopicId: number;
  countElements: number;
}

export class Topic {
  private _id: number;
  private _title: string;
  private _description: string;
  private _number: number;
  private _difficult: number;
  private _isAdded: boolean = false;
  private _lTopicId: number = 0;
  private _countElements: number;

  constructor(id: number, title: string, description: string, number: number, difficult: number, countElements: number) {
    this._id = id;
    this._title = title;
    this._description = description;
    this._number = number;
    this._difficult = difficult;
    this._countElements = countElements;
  }

  static toModel(dto: TopicDto): Topic {
    let topic = new Topic(dto.id, dto.title, dto.description, dto.number, dto.difficult, dto.countElements);
    topic._isAdded = dto.added;
    topic._lTopicId = dto.lTopicId;
    return topic;
  }
  
  get id(): number {
    return this._id;
  }
  
  set id(value: number) {
    this._id = value;
  }
  
  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get description(): string {
    return this._description;
  }
  
  set description(value: string) {
    this._description = value;
  }
  
  get number(): number {
    return this._number;
  }
  
  set number(value: number) {
    this._number = value;
  }
  
  get difficult(): number {
    return this._difficult;
  }
  
  set difficult(value: number) {
    this._difficult = value;
  }

  get added(): boolean {
    return this._isAdded;
  }

  set added(value: boolean) {
    this._isAdded = value;
  }

  get lTopicId(): number {
    return this._lTopicId;
  }

  set lTopicId(value: number) {
    this._lTopicId = value;
  }

  get countElements(): number {
    return this._countElements;
  }
  set countElements(value: number) {
    this._countElements = value;
  }
}
