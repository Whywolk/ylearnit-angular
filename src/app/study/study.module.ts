import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StudyRoutingModule } from './study-routing.module';
import { CourseDetailsComponent } from './component/course-details/course-details.component';
import { CoursesListComponent } from './component/courses-list/courses-list.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { HomeComponent } from './component/home/home.component';
import { LearningCardDetailsComponent } from './component/learning-card-details/learning-card-details.component';
import { LearningCardListComponent } from './component/learning-card-list/learning-card-list.component';
import { StudyTestComponent } from './component/study-test/study-test.component';
import { StudyTestsComponent } from './component/study-tests/study-tests.component';
import { TestDirective } from './answer.directive';
import { SM2TestComponent } from './component/sm2-test/sm2-test.component';
import { WriteTestComponent } from './component/write-test/write-test.component';
import { StudyTestsResultsComponent } from './component/study-tests-results/study-tests-results.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { TopicDetailsComponent } from './component/topic-details/topic-details.component';
import { TranslocoRootModule } from '../transloco-root.module';



@NgModule({
  declarations: [
    HomeComponent,
    CoursesListComponent,
    CourseDetailsComponent,
    DashboardComponent,
    StudyTestComponent,
    StudyTestsComponent,
    LearningCardListComponent,
    LearningCardDetailsComponent,
    TestDirective,
    SM2TestComponent,
    WriteTestComponent,
    StudyTestsResultsComponent,
    NavbarComponent,
    TopicDetailsComponent
  ],
  imports: [
    CommonModule,
    StudyRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslocoRootModule
  ]
})
export class StudyModule { }
