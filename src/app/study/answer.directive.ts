import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[studyAnswerTest]'
})
export class TestDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
