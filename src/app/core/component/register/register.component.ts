import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../custom-validators';
import { LoggerService } from '../../service/logger.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  private log = new LoggerService(this);

  registerForm: FormGroup;

  alertMessage = '';

  isShowPassword1 = false;
  isShowPassword2 = false;

  constructor(
    private authService: AuthService, 
    private router: Router, 
    private fb: FormBuilder) {
    this.registerForm = this.fb.group({
      username: new FormControl('', {
        validators: Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(25),
          CustomValidators.patternValidator(/^[A-Za-z0-9_#?!&*~-]+$/, { pattern: true })
        ]),
        updateOn: 'change'
      }),
      password: new FormControl('', {
        validators: Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(40),
          CustomValidators.patternValidator(/[A-Z]/, { upperCase: true }),
          CustomValidators.patternValidator(/[a-z]/, { lowerCase: true }),
          CustomValidators.patternValidator(/[0-9]/, { number: true }),
          CustomValidators.patternValidator(/[_#?!@$%^&*-]/, { specialSymbol: true }),
          CustomValidators.patternValidator(/^(?=.*[A-Za-z0-9_#?!@$%^&*-])[A-Za-z0-9_#?!@$%^&*-]+$/, { allSymbols: true }),
          CustomValidators.patternValidator(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[_#?!@$%^&*-])[A-Za-z0-9_#?!@$%^&*-]+$/, { pattern: true })
        ]),
        updateOn: 'change'
      }),
      confirmPassword: new FormControl('', {
        validators: Validators.compose([Validators.required]),
        updateOn: 'change'
      })
    },
      { validator: CustomValidators.passwordMatchValidator }
    );
  }

  get username() {
    return this.registerForm.get('username');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  onRegister(): void {
    this.log.info('onRegister');
    if (this.password?.value === this.confirmPassword?.value) {
      this.authService.register(this.username?.value, this.password?.value)
        .subscribe({
          next: (data: string) => {
            this.alertMessage = '';
            this.router.navigate(['/app']);
          },
          error: (e) => {
            this.log.warn(e);
            this.alertMessage = e.message;
          }
        });
    }
  }

  passwordValid(): boolean {
    if (this.password?.hasError('required') || this.password?.hasError('minlength') || this.password?.hasError('maxlength')) {
      return false;
    }

    return this.passwordValidPatterns();
  }

  passwordValidPatterns(): boolean {
    let rules = ['upperCase', 'lowerCase', 'number', 'specialSymbol'];
    let count = 0;

    for (let rule of rules) {
      if (!this.password?.hasError(rule)) {
        count += 1;
      }
    }
    
    return count >= 2 && !this.password?.hasError('allSymbols');
  }

  confirmPasswordValid(): boolean {
    return !this.confirmPassword?.hasError('required');
  }

  passwordsValid(): boolean {
    return this.passwordValid() && this.confirmPasswordValid() && this.password?.value === this.confirmPassword?.value;
  }
}
