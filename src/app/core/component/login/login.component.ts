import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoggerService } from '../../service/logger.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  private log = new LoggerService(this);
  loginForm: FormGroup;

  isShowPassword = false;
  rememberMe = false;
  loginAlert = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) {
    this.loginForm = this.fb.group({
      username: new FormControl('', {
        validators: Validators.compose([Validators.required]),
        updateOn: 'change'
      }),
      password: new FormControl('', {
        validators: Validators.compose([Validators.required]),
        updateOn: 'change'
      })
    });
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onLogin(): void {
    this.log.debug('onLogin');
    this.authService.login(this.username?.value, this.password?.value, this.rememberMe)
      .subscribe({
        next: (data: string) => {
          this.loginAlert = false;
          this.router.navigate(['/app']);
        },
        error: (e) => {
          this.log.warn(e);
          this.loginAlert = true;
        }
      });
  }
}
