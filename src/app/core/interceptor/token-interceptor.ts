import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError, throwError } from "rxjs";
import { AuthService } from "../service/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { LoggerService } from "../service/logger.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private log = new LoggerService(TokenInterceptor);

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('/api/auth') || req.url.includes('assets/app-config.json')) {
      // do nothing
      return next.handle(req);
    }
    
    if (! this.authService.isAuth()) {
      this.log.debug(req.url);
      this.router.navigate(['']);
      // throw new Error('Token expired');
    }
    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    });
    return next.handle(req);
      // .pipe(
      //   catchError((e: HttpErrorResponse) => {
      //     console.warn(e);
      //     this.router.navigate(['']);
      //     return throwError(() => {return e});
      //   })
      // );
  }
}