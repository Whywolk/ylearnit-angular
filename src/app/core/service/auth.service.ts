import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, lastValueFrom, tap } from "rxjs";
import { JwtService } from './jwt.service';
import { AppConfigService } from 'src/app/app-config.service';
import { LoggerService } from './logger.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private log = new LoggerService(this);

  private authUrl = '/auth';

  constructor(
    private appConfig: AppConfigService, 
    private http: HttpClient, 
    private jwtService: JwtService) { }

  login(username: string, password: string, rememberMe: boolean): Observable<any> {
    return this.http.post(`${this.getApiUrl()}/login`, { 'login': username, 'password': password, 'rememberMe': rememberMe })
      .pipe(
        tap(data => {
          this.saveToken(data, rememberMe);
        }));
  }

  register(username: string, password: string): Observable<any> {
    return this.http.post(`${this.getApiUrl()}/register`, { 'login': username, 'password': password, 'rememberMe': false })
      .pipe(
        tap(data => {
          this.saveToken(data, false);
        }));
  }


  refresh(): Observable<any> {
    return this.http.post(`${this.getApiUrl()}/refresh`, { 'refresh': localStorage.getItem('refreshToken') })
      .pipe(
        tap(data => {
          this.saveToken(data, true);
        }));
  }

  logout(): void {
    this.clearStorage();

    // TODO: http req for clear http-only cookies
  }

  isAuth(): boolean {
    try {
      let token = localStorage.getItem('accessToken');
      if (token) {
        let payload = this.jwtService.parseJwt(token);
        return !this.jwtService.isExpired(payload);
      } else {
        return false;
      }
    } catch (e) {
      console.warn(e);
      return false;
    }
  }

  get username(): string {
    return localStorage.getItem('username')!;
  }

  private saveToken(token: any, rememberMe: boolean): void {
    localStorage.setItem('accessToken', token['access']);
    if (rememberMe) {
      localStorage.setItem('refreshToken', token['refresh']);
    }

    let payload = this.jwtService.parseJwt(token['access']);
    if (payload) {
      localStorage.setItem('username', payload['sub']);
    }
    this.log.info('Token saved');
  }

  private clearStorage(): void {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('username');
    this.log.info('Logout, clear storage');
  }

  private getApiUrl(): string {
    return this.appConfig.apiUrl + this.authUrl;
  }
}
