import { Injectable } from '@angular/core';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private log = new LoggerService(this);

  private isClientClockOffset = false;
  private clockOffsetMinutes = 5;

  constructor() { }

  parseJwt(jwt: string): any {
    let splitedToken = jwt.split('.');
    let payload = JSON.parse(window.atob(splitedToken[1]));
    return payload;
  }

  isExpired(payload: any): boolean {
    let curDate = new Date();

    // unit-time is seconds, Date is milliseconds, so Date = unix * 1000
    let iatDate = new Date(payload['iat'] * 1000);
    let expDate = new Date(payload['exp'] * 1000);
    
    this.log.debug('ttl = ' +  ((expDate.getTime() - curDate.getTime()) / 1000));
    if (this.isClientClockOffset) {
      iatDate.setMinutes(iatDate.getMinutes() - this.clockOffsetMinutes);
      expDate.setMinutes(expDate.getMinutes() - this.clockOffsetMinutes);
    }

    return (curDate < iatDate) || (curDate > expDate);
  }
}
