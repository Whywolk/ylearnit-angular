enum Level {
  DEBUG = 0,
  INFO = 1,
  WARN = 2,
  ERROR = 3,
  OFF = 4
}

export class LoggerService {
  private typeName: String;

  constructor(type: Object) {
    this.typeName = type.constructor.name;
  }

  debug(m: any): void {
    this.logWith(Level.DEBUG, m);
  }

  info(m: any): void {
    this.logWith(Level.INFO, m);
  }

  warn(m: any): void {
    this.logWith(Level.WARN, m);
  }

  error(m: any): void {
    this.logWith(Level.ERROR, m);
  }

  private logWith(level: Level, m: any): void {
    if (typeof m == 'string') {
      let l = Level[level];
      let message = `[${this.getTime()}] ${l} ${this.typeName} - ${m}`;
      switch (level) {
        case Level.DEBUG:
          return console.debug(message);
        case Level.INFO:
          return console.info(message);
        case Level.WARN:
          return console.warn(message);
        case Level.ERROR:
          return console.error(message);
      }
    }
    switch (level) {
      case Level.DEBUG:
        return console.debug(m);
      case Level.INFO:
        return console.info(m);
      case Level.WARN:
        return console.warn(m);
      case Level.ERROR:
        return console.error(m);
    }
  }

  private getTime(): string {
    let d = new Date();
    let hours = String(d.getHours()).padStart(2, '0');
    let minutes = String(d.getMinutes()).padStart(2, '0');
    let seconds = String(d.getSeconds()).padStart(2, '0');
    let milliseconds = String(d.getMilliseconds()).padStart(3, '00');
    return `${hours}:${minutes}:${seconds}.${milliseconds}`;
  }
}
