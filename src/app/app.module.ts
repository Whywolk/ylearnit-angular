import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppConfigService } from './app-config.service';
import { httpInterceptorProviders } from './http-interceptor';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/component/login/login.component';
import { RegisterComponent } from './core/component/register/register.component';
import { TranslocoRootModule } from './transloco-root.module';

export function appConfigInit(appConfigService: AppConfigService) {
  return () => {
    return appConfigService.load();
  }
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    TranslocoRootModule
  ],
  exports: [],
  providers: [
    { provide: APP_INITIALIZER, useFactory: appConfigInit, multi: true, deps: [AppConfigService] },
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
